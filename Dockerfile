#
# Enhanced Buildroot Dockerfile
#

# Aguments (preprocessors)
ARG BUILDROOT_IMAGE=latest

FROM buildroot/base:${BUILDROOT_IMAGE:-latest} AS builder

## Environment variable.
ENV DEBIAN_FRONTEND noninteractive

## Switch to root user.
USER root

## Update APT cache.
RUN apt update

## Install OpenSSH-Client.
RUN apt -y install openssh-client

## Cleanup APT cache.
RUN apt clean

## Switch back to br-user.
USER br-user